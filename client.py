import sys

from twisted.internet import reactor, defer
from twisted.python import log

from rlmclient.rlmclient import RLMClient

if __name__ == '__main__':
    log.startLogging(sys.stdout)
    rlmclient = RLMClient()
    d = defer.Deferred()
    d.addBoth(rlmclient.start)
    reactor.callWhenRunning(d.callback, 0)
    reactor.run()
