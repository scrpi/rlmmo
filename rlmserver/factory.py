from twisted.internet.protocol import ServerFactory

from rlmserver.protocol import RLMProtocol

class RLMFactory(ServerFactory):
    """
    TODO: Doco
    """
    protocol = RLMProtocol

    # TODO call teardown_protocol from protocol

    def __init__(self, service):
        self.service = service
        service.factory = self

        self.protocols = dict()

    def register_protocol(self, protocol):
        username = protocol.username
        self.protocols[username] = protocol
    
    def teardown_protocol(self, protocol):
        """ Do internal stuff on behalf of a protocol which has been
        disconnected.

        """
        username = protocol.username

        if username in self.protocols:
            del self.protocols[username]
