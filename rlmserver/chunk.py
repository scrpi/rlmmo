import os
import random

from multiprocessing import Process

from twisted.internet import reactor

from construct import Struct, Array, UBInt8
from construct import UBInt16, Container, Bit

import libtcod.libtcodpy as libtcod

from rlmserver.packets import make_packet
from rlmserver.mapdata import tiles
from rlmserver.generator import OWGen

try:
    import numpypy
except:
    pass
import numpy

chunk_struct = Struct('chunk',
                      Array(64, Array(64, UBInt16('tiles'))),
                      Array(64, Array(64, Bit('blocks_movement'))),
                      Array(64, Array(64, UBInt8('biome'))),
                      Array(64, Array(64, UBInt8('height'))),
                      )

packet_struct = Struct('chunk',
                       Array(64, Array(64, UBInt16('tiles'))),
                       Array(64, Array(64, Bit('blocks_movement'))),
                       Array(64, Array(64, UBInt8('biome'))),
                       Array(64, Array(64, UBInt8('height'))),
                       )

class Chunk(object):
    """ A chunk of cells."""

    CHUNK_DIR = "data/chunks"  # Move this to another spot?

    def __init__(self, zid, x, y):
        """
        :param int zid: zone id that this chunk belongs to.
        :param int x: X coordinate in chunk coords
        :param int y: Y coordinate in chunk coords
        """

        self.zid = int(zid)
        self.x = int(x)
        self.y = int(y)

        self.zone_dir = "{}/{}/".format(self.CHUNK_DIR, self.zid)

        self.x_dir = "{}x{}/".format(self.zone_dir, self.x)

        # File path prefix of where this chunk is stored on disk
        self.filename = "{}cnk_{}_{}".format(self.x_dir, self.x, self.y)
        self.chunk_file = self.filename + ".bin"

        # Set all file path locations
        #self.tiles_file = "{}.tiles.npy".format(self.file_prefix)

        self.chunk_checks = 0

        self.ready = False
        self.generating = False

        self.load()


    def load(self):
        """ Loads a chunk from file if it exists """

        if os.path.exists(self.chunk_file):
            fp = open(self.chunk_file, "rb")  # Lets make nonblocking later
            data = fp.read()
            fp.close()

            c = chunk_struct.parse(data)
            self.tiles = numpy.array(c.tiles, dtype="uint16")
            self.blocks_movement = numpy.array(c.blocks_movement, dtype="bool")
            self.biome = numpy.array(c.biome, dtype="uint8")
            self.height = numpy.array(c.height, dtype="uint8")

            if not self.generating: self.ready = True
        else:
            self.generating = True
            self._generate()


    def save(self):
        """ Saves a chunk to file """

        # Check that the zone_dir exists, if not create.
        if not os.path.exists(self.x_dir):
            os.makedirs(self.x_dir)

        c = Container(tiles = self.tiles.tolist(),
                      blocks_movement = self.blocks_movement.tolist(),
                      biome = self.biome.tolist(),
                      height = self.height.tolist(),
                      )
        data = chunk_struct.build(c)

        fp = open(self.chunk_file, "wb")  # Lets make this non-blocking later.
        fp.write(data)
        fp.close()

    def default_blocks_movement(self):
        for x in range(64):
            for y in range(64):
                t = self.tiles[x,y]
                if tiles[t].blocks_movement:
                    self.blocks_movement[x,y] = 1
                else:
                    self.blocks_movement[x,y] = 0


    def cell_dump(self, zone_coord):
        """ Receives a zone coordinate and returns a Container with
        information about this cell to be sent to the client"""

        zone_x, zone_y = zone_coord
        i_x = zone_x % 64
        i_y = zone_y % 64

        c = Container(x = zone_x, 
                      y = zone_y,
                      tile = self.tiles[i_x, i_y],
                      blocks_movement = self.blocks_movement[i_x, i_y],
                      biome = self.biome.tolist(),
                      height = self.height.tolist(),
                      )
        return (c)


    def save_to_packet(self):
        """
        Generate a chunk packet.
        """

        if self.ready:
            c = Container(tiles = self.tiles.tolist(),
                          blocks_movement = self.blocks_movement.tolist(),
                          biome = self.biome.tolist(),
                          height = self.height.tolist(),
                          )
            return packet_struct.build(c)


    def _generate(self):
        # Check that the zone_dir exists, if not create.
        if not os.path.exists(self.x_dir):
            os.makedirs(self.x_dir)
        
        g = OWGen(self.zid + 9002,
                  self.filename)

        #g.render(64, 64, self.x, self.y, 0.5)

        p = Process(target = g.render, args=(64, 64, self.x, self.y, 0.25))
        p.start()
        reactor.callLater(0.5, self._check_generated_chunk)

    def _check_generated_chunk(self):
        self.chunk_checks += 1
        if os.path.exists(self.chunk_file):
            self.load()
            self.default_blocks_movement()
            self.save()
            self.ready = True
            self.generating = False
            print "Generated new chunk({}, {}) in zone {}".format(self.x, self.y, self.zid)
        elif self.chunk_checks < 50:
            reactor.callLater(0.2, self._check_generated_chunk)
        
