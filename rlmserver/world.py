from twisted.internet.task import LoopingCall
from twisted.python import log

from rlmserver.zone import Zone


class World:
    
    def __init__(self):
        self.ticks = 0
        self.zones = dict()

    def start(self):
        # Load a test zone
        #self.load_zone(1)
        
        # Set up a looping call every 1/10th of a second to run game tick
        DESIRED_FPS = 10.0 # 10 frames per second
        self.tick = LoopingCall(self.game_tick)
        self.tick.start(1.0 / DESIRED_FPS)

    def game_tick(self):
       #events = pygame.events.get()
       #for event in events:
          # Process input events
       #redraw()
       self.ticks += 1
       #if self.ticks % 100 == 0: log.msg('Tick: %d' % self.ticks)

    def load_zone(self, zid):
        """
        Loads a zone into memory
        """

        if zid not in self.zones:
            log.msg('Loading a new zone into memory: {}'.format(zid))
            z = Zone(self, zid)
            self.zones[zid] = z
            log.msg('Zone {} loaded'.format(zid))

        return self.zones[zid]
