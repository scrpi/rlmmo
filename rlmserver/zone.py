import cPickle as pickle
import zlib

from twisted.python import log
from twisted.internet import reactor

from rlmserver.chunk import Chunk
from rlmserver.packets import make_packet

CACHE_RADIUS = 30  # We might want to change how this is defined later

class Zone:

    """
    Zone - an object that represents one instance of a distinct area in the 
    world. Consists of a map and references to all entities inside etc.
    """

    width = 40
    height = 40
    """
    The size of the zone map.
    """

    name = "testzone"
    """
    The name of the zone to be sent to the client
    """

    protocols = {}
    """
    Dictionary containing username:protocol of all players in this zone
    """

    def __init__(self, world, zid):
        self.world = world
        self.zid = zid

        # Dictionary of all chunks currently loaded in memory
        self.chunks = dict()
        self.requested_chunks = []

        # Create the zone map
        #self.map = [[ Cell() 
        #    for y in range(self.height) ]
        #        for x in range(self.width) ]

    def player_move(self, protocol, x, y):
        """ Player has asked to move to x,y """

        # Check if the location is blocked
        chunk_coord = self.get_chunk_coords((x, y))
        i_x, i_y = self.get_internal_coords((x, y))
        b_m = self.chunks[chunk_coord].blocks_movement[i_x, i_y]
        if not self.chunks[chunk_coord].blocks_movement[i_x, i_y]:
            protocol.player_x = x
            protocol.player_y = y
            self.send_chunks(protocol)
            #protocol.location_update()

            # Send entity location update
            packet = make_packet("player_teleport",
                                 username = protocol.username,
                                 x = x,
                                 y = y)
            self.broadcast_for_others(protocol, packet)
        else:
            protocol.location_update()


    def player_disconnect(self, protocol):
        if self.protocols.has_key(protocol.username):
            del self.protocols[protocol.username]
        packet = make_packet("player_destroy", username = protocol.username)
        self.broadcast(packet)

    def broadcast(self, packet):
        for player in self.protocols.itervalues():
            player.transport.write(packet)

    def broadcast_for_others(self, protocol, packet):
        for player in self.protocols.itervalues():
            if player is not protocol:
                player.transport.write(packet)

    def send_chunks(self, protocol):
        """ Grab all surrounding chunks and send to the player. """
        cx, cy = self.get_chunk_coords((protocol.player_x,protocol.player_y))

        for x in xrange(cx-1, cx+2):
            for y in xrange(cy-1, cy+2):
                self.load_chunk((x,y))
                if (x,y) not in protocol.client_chunk_cache:
                    if self.chunks[(x,y)].ready == True:
                        pkt = self.chunks[(x,y)].save_to_packet()
                        protocol.write_packet("chunk", x = x, y = y, data = pkt)
                        protocol.client_chunk_cache.append((x,y))
                    elif (x,y) not in self.requested_chunks:
                        self.requested_chunks.append((x,y))
                        reactor.callLater(0.2, self.send_chunk_later, (x,y), protocol)

    def send_chunk_later(self, c, protocol):
        if c in self.chunks:
            if self.chunks[c].ready == True:
                x, y = c
                pkt = self.chunks[(x,y)].save_to_packet()
                protocol.write_packet("chunk", x = x, y = y, data = pkt)
                protocol.client_chunk_cache.append((x,y))
                for r in self.requested_chunks:
                    if r == c:
                        self.requested_chunks.remove(r)
            else:
                reactor.callLater(0.2, self.send_chunk_later, c, protocol)

    def load_chunk(self, chunk_coords):
        chunk_x, chunk_y = chunk_coords
        if chunk_coords not in self.chunks:
            log.msg('Loading new chunk {} in zone {}'.format(chunk_coords,
                                                               self.zid))
            self.chunks[chunk_coords] = Chunk(self.zid, chunk_x, chunk_y)


    def get_chunk_coords(self, coord):
        """ Takes a zone coord in (x,y) and returns chunk coords """

        x, y = coord
        return (x / 64, y / 64)

    def get_internal_coords(self, coord):
        """ Takes a zone coord in (x,y) and returns chunk internal coords """

        x,y = coord
        return(x % 64, y % 64)

