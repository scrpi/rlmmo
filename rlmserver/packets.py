from construct import Struct, Container, Switch, Range
from construct import OptionalGreedyRange, PascalString
from construct import UBInt8, UBInt16, UBInt32, UBInt64
from construct import SBInt8, SBInt16, SBInt32, SBInt64
from construct import Bit, Array

from twisted.python import log

DUMP_ALL_PACKETS = False

# The actual packet list.
packets = {
    # C > S
    0: Struct("loginrequest",
              PascalString("username"),
              PascalString("token"),  # TODO Some kind of token for authentication?
              UBInt32("protocol_version"),
    ),
    # C < S
    1: Struct("loginaccept",
              UBInt64("entity_id"),
    ),
    # C < S
    20: Struct("newzone",  # Player login or go to new zone. Client redraws map
               PascalString("name"),  # The zone name to be displayed in the client
               UBInt16("player_count"),  # current number of players in zone
               UBInt32("width"),  # zone width - is this necessary?
               UBInt32("height"),  # see above
               SBInt32("x"),  # Coords to spawn the player character
               SBInt32("y"),
    ),
    # C < S
    21: Struct("sendcells",
               UBInt32("num"),
               Array(lambda ctx: ctx.num, 
                   Struct("celldata",
                          SBInt32("x"),
                          SBInt32("y"),
                          UBInt8("tile"),
                          UBInt8("hm"),
                          Bit("blocks_movement"),
                   )
               )
    ),
    # C < S
    30: Struct("chunk",
               SBInt32("x"),
               SBInt32("y"),
               PascalString("data",
                            length_field=UBInt32("length"),
               ),
    ),
    # C <> S
    40: Struct("location",
               SBInt32("x"),
               SBInt32("y"),
    ),
    # C < S
    41: Struct("player_teleport",
               PascalString("username"),
               SBInt32("x"),
               SBInt32("y"),
    ),
    # C < S
    42: Struct("player_destroy",
               PascalString("username"),
    ),
    # C <> S
    254: Struct("ping",
                UBInt32("pid"),  # Server sends a timestamp, client responds
                                 # with same packet.
    ),
    # C < S
    255: Struct("error",
                UBInt8("type"),  # 0 = error, 1 = kick 
                PascalString("message"), 
    ),
}

packet_stream = Struct("packet_stream",
    OptionalGreedyRange(
        Struct("full_packet",
            UBInt8("header"),
            Switch("payload", lambda context: context["header"], packets),
        ),
    ),
    OptionalGreedyRange(
        UBInt8("leftovers"),
    ),
)

def parse_packets(bytestream):
    """ Opportunistically parse out as many packets as possible from a raw
    bytestream.

    Returns a tuple containing a list of unpacked packet containers, and any
    leftover unparseable bytes.
    """
    container = packet_stream.parse(bytestream)
    l = [(i.header, i.payload) for i in container.full_packet]
    leftovers = "".join(chr(i) for i in container.leftovers)
    if DUMP_ALL_PACKETS:
        for packet in l:
            log.msg('Received and parsed packet %d' % packet[0])
            log.msg('    %s' % str(packet[1]))
    return l, leftovers

packets_by_name = dict((v.name, k) for (k, v) in packets.iteritems())

def make_packet(packet, *args, **kwargs):
    """
    Constructs a packet bytestream from a packet header and payload.

    The payload should be passed as keyword arguments. Additional containers
    or dictionaries to be added to the payload may be passed positionally, as
    well.
    """

    if packet not in packets_by_name:
        log.msg('Couldn\'t find packet name %s!' % packet)
        return ""

    header = packets_by_name[packet]

    for arg in args:
        kwargs.update(dict(arg))
    container = Container(**kwargs)

    if DUMP_ALL_PACKETS:
        log.msg('Making packet %s (%d)' % (packet, header))
        log.msg('    %s' % str(container))
    payload = packets[header].build(container)
    return chr(header) + payload
