import Image
import numpy

from simplex import Simplex
from mapdata import tiles

from construct import Struct, Array, UBInt8
from construct import UBInt16, Container, Bit

chunk_struct = Struct('chunk',
                      Array(64, Array(64, UBInt16('tiles'))),
                      Array(64, Array(64, Bit('blocks_movement'))),
                      Array(64, Array(64, UBInt8('biome'))),
                      Array(64, Array(64, UBInt8('height'))),
                      )

(BIOME_DESERT, BIOME_GRASS, BIOME_SNOW) = range(3)
(HEIGHT_OCEAN, HEIGHT_LAND, HEIGHT_MNTN) = range(3)

class OWGen(object):

    def __init__(self, seed = 0, filename = "owgen", test = False):

        self.height  = Simplex()
        self.biome   = Simplex()
        self.terrain = Simplex()

        self.height.set_seed(seed)
        self.biome.set_seed(seed + 31415)
        self.terrain.set_seed(seed + 1234)

        self.filename = filename
        self.chunk_file = filename + ".bin"

        self.chunk_tiles = numpy.zeros((64, 64), dtype="uint16")
        self.chunk_biome = numpy.zeros((64, 64), dtype="uint8")
        self.chunk_height = numpy.zeros((64, 64), dtype="uint8")

        self.test = test

    def render(self, w, h, x = 0, y = 0, zoom = 2.0): 

        if w == None or h == None:
            return

        if w != 64 or h != 64:
            self.test = True

        zx = x * 64
        zy = y * 64

        im = Image.new("RGB", (w, h))

        for x in xrange(w):
            for y in xrange(h):
                val = self.height.octaves2((zx+x)/(1024.0*zoom), (zy+y)/(1024.0*zoom), 6)
                if val < -1: val = -1
                if val > 1: val = 1

                b = self.biome.octaves2((zx+x)/(2048.0*zoom), (zy+y)/(2048.0*zoom), 6)
                if b < -1: b = -1
                if b > 1: b = 1

                t = self.terrain.octaves2((zx+x)/(128.0*zoom), (zy+y)/(128.0*zoom), 6)
                if t < -1: t = -1
                if t > 1: t = 1

                # Biomes
                btype = BIOME_DESERT  # Arid Desert
                if b > -0.50: btype = BIOME_GRASS  # Grassland
                if b > 0.50: btype = BIOME_SNOW  # Snow 

                # Ocean
                elevation = HEIGHT_OCEAN
                tile = 100
                if val > -0.90: tile = 101
                if val > -0.85: tile = 102
                if val > -0.80: tile = 103
                if val > -0.75: tile = 104
                if val > -0.70: tile = 105
                if val > -0.65: tile = 106
                if val > -0.60: tile = 107
                if val > -0.55: tile = 108
                if val > -0.50: tile = 109

                # Coastal
                if val > -0.45:
                    elevation = HEIGHT_LAND
                    if btype == 0: tile = 200
                    if btype == 1: tile = 200
                    if btype == 2: tile = 1000

                # Land
                if val > -0.40:
                    if btype == 0: 
                        tile = 200
                        if t > -0.6: tile = 201
                        if t > -0.2: tile = 202
                        if t > 0.2:  tile = 203
                        if t > 0.6:  tile = 204

                    if btype == 1: 
                        tile = 500
                        if t > -0.60:  tile = 501
                        if t > -0.45:  tile = 502
                        if t > -0.30:  tile = 503
                        if t > -0.15:  tile = 504
                        if t > 0.00:  tile = 505
                        if t > 0.15:  tile = 506
                        if t > 0.30:  tile = 507
                        if t > 0.45:  tile = 508
                        if t > 0.60:  tile = 509
                        if t > 0.65:  tile = 510
                        if t > 0.70:  tile = 511
                        if t > 0.75:  tile = 512
                        if t > 0.80:  tile = 513
                        if t > 0.85:  tile = 514
                        if t > 0.90:  tile = 515
                        if t > 0.92:  tile = 516
                        if t > 0.94:  tile = 517
                        if t > 0.96:  tile = 518
                        if t > 0.97:  tile = 519
                        if t > 0.975:  tile = 520
                        if t > 0.980:  tile = 521
                        if t > 0.985:  tile = 522
                        if t > 0.990:  tile = 524
                        if t > 0.995:  tile = 525

                    if btype == 2: 
                        tile = 1000
                        if t > -0.67: tile = 1001
                        if t > -0.34: tile = 1002
                        if t > 0.00:  tile = 1003
                        if t > 0.33:  tile = 1004
                        if t > 0.67:  tile = 1005

                # Mountains
                if val > 0.60:
                    elevation = HEIGHT_MNTN

                # Save tile data
                if not self.test:
                    self.chunk_tiles[x,y] = tile
                    self.chunk_biome[x,y] = btype
                    self.chunk_height[x,y] = elevation

                # Generate the PNG
                if x % 64 == 0 or y % 64 == 0 and self.test:
                    im.putpixel((x,y), (0,0,0))
                else:
                    col = tiles[tile].back()
                    if elevation == HEIGHT_MNTN:
                        r,g,b = col
                        r = int(r * 0.3)
                        g = int(g * 0.3)
                        b = int(b * 0.3)
                        col = (r,g,b)
                    im.putpixel((x,y), col)

        # Save the png
        png = self.filename + ".png"
        if self.test: print "Saving to PNG - {}".format(png)
        im.save(png)

        # Save the chunk data
        if not self.test:
            self.save()

    def save(self):
        """ Saves a chunk to file """

        bm = numpy.zeros((64,64), dtype="bool")

        c = Container(tiles = self.chunk_tiles.tolist(),
                      blocks_movement = bm.tolist(),
                      biome = self.chunk_biome.tolist(),
                      height = self.chunk_height.tolist(),
                      )
        data = chunk_struct.build(c)

        fp = open(self.chunk_file, "wb")  # Lets make this non-blocking later.
        fp.write(data)
        fp.close()

if __name__ == "__main__":

    seed = 4024182

    zoom = 0.4

    W = 1280
    H = 1280

    g = OWGen(seed, "test")
    g.render(W, H, 0, 0, zoom)
