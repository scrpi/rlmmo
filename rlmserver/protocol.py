import time
import random

from twisted.internet.protocol import Protocol
from twisted.internet.task import LoopingCall
from twisted.internet import reactor
from twisted.python import log

from rlmserver.packets import parse_packets, make_packet

from utilities.temporal import timestamp_from_clock

class RLMProtocol(Protocol):
    """
    This class represents a connection to a single client. This is where the
    state of the connected user/player is.

    """
    buf = ""
    username = None

    def __init__(self):
        self.handlers = {
            0: self.loginrequest,
            40: self.location,
            254: self.ping,
        }

        self._ping_loop = LoopingCall(self.update_ping)
        
        self.needs_cell_sync = False

        self.packet_queue = []
        self.sending_queue = False

        self.client_chunk_cache = []

    ##################################
    # Client > Server packet handlers

    def ping(self, container):
        """
        Handle a ping packet from a client.

        """
        now = timestamp_from_clock(reactor)
        self.latency = now - container.pid
        log.msg("Ping? Pong! {} Latency: {}ms".format(self.username, self.latency))

    def loginrequest(self, container):
        # Check if this username is already logged in
        # TODO Replace this section with some sort of authentication
        if container.username in self.factory.protocols:
            # Username is already loggined in, send error.
            log.msg('%s username already in protocols' % container.username)
            log.msg(self.factory.protocols)
            self.error(0, 'This username is taken')
            return

        # Username is not logged in, proceed.
        self.username = container.username
        self.token = container.token
        self.protocol_version = container.protocol_version
        self.factory.register_protocol(self)

        # TODO: We should load player from persistant storage here.
        # Determine what zone the player is currently in.
        # If the player was in a temporary zone (like a dungeon) then we will
        # need to spawn them outside the entrance

        # For now, load a test zone and send to the player
        self.cell_cache = []
        self.player_x = random.randint(-5, 5)
        self.player_y = random.randint(-5, 5)
        self.zone = self.world.load_zone(1)
        self.zone.protocols[self.username] = self
        print self.zone.protocols
        self.new_zone()  # Sends new zone pkt to the client to spawn character
        #self.needs_cell_sync = True
        #self._sync_loop = LoopingCall(self.zone.sync_cells, self)
        #self._sync_loop.start(1)  # Sends map data to the client

        # Get the chunks surrounding this player and send.
        self.zone.send_chunks(self)

        # Get the players in this zone and send.
        for player in self.zone.protocols.itervalues():
            if not player == self:
                self.write_packet("player_teleport",
                                  username = player.username,
                                  x = player.player_x,
                                  y = player.player_y)

        # Send me to other players
        packet = make_packet("player_teleport",
                             username = self.username,
                             x = self.player_x,
                             y = self.player_y)
        self.zone.broadcast_for_others(self, packet)

        # Simulate the player moving some cells to the right
        #reactor.callLater(0.5, self.test_move, 1, 0)

        # Init player, and copy data into it.
        #self.player = yield self.factory.world.load_player(self.username)
        #self.player.eid = self.eid
        #self.location = self.player.location

    def location(self, container):
        """ Handle a location update from the client """

        self.zone.player_move(self, container.x, container.y)


    ##################################
    # Server > Client packet handlers

    def update_ping(self):
        """
        Send a ping to the client.
        """

        now = timestamp_from_clock(reactor)
        self.write_packet("ping", pid=now)

    def error(self, type, message):
        """
        This will send an error message to the client and disconnect them.

        type -- the type of error (0=error, 1=kick)
        message -- string passed to the client to describe the error
        """

        self.write_packet("error", type=type, message=message)
        self.transport.loseConnection()

    def new_zone(self):
        """
        Send a packet to the client telling them to load a new zone. This could
        be done when the client first connects or when travelling between
        zones.
        """
        log.msg('Starting to send new zone: %s' % str(self.zone))
        self.write_packet("newzone", name = self.zone.name, player_count =
                          len(self.zone.protocols), width = self.zone.width,
                          height = self.zone.height, x = self.player_x, y =
                          self.player_y)

    def location_update(self):
        """ Sends a location packet to the client"""
        
        self.write_packet("location", x = self.player_x, y = self.player_y)


    ##################################
    # Connection methods

    def connectionMade(self):
        self.world = self.factory.service.world
        log.msg('Received connection from %s'% self.transport.getPeer())

        # Start up the ping loop.
        reactor.callLater(10, self._ping_loop.start, 10)

    def connectionLost(self, reason):
        self.zone.player_disconnect(self)
        self.factory.teardown_protocol(self)
        
        if self._ping_loop.running:
            self._ping_loop.stop()

        log.msg('Connection dropped from %s'% self.transport.getPeer())

    def dataReceived(self, data):
        self.buf += data

        packets, self.buf = parse_packets(self.buf)

        if packets:
            # TODO ping loop timeout
            # self.resetTimeout()
            pass

        for header, payload in packets:
            if header in self.handlers:
                self.handlers[header](payload)
            else:
                log.err("Didn't handle parseable packet %d!" % header)
                log.err(payload)

    def write_packet(self, header, **payload):
        """
        Send a packet to the client.

        """
        self.transport.write(make_packet(header, **payload))
