from twisted.application import internet, service
from twisted.python import log

from rlmserver.factory import RLMFactory
from rlmserver.world import World

class RLMService(service.Service):
    """
    Return a service suitable for creating an application object.

    This service is a game server. port 1234
    """
    
    factory = None
    
    def __init__(self):
        pass
        
    def startService(self):
        self.world = World()
        self.world.start()
        log.msg('Service Started with factory %s' % str(self.factory))
        
        

# configuration parameters
port = 1234
iface = 'localhost'

application = service.Application("rlmmo")

rlmService = RLMService()
rlmService.setServiceParent(application)

factory = RLMFactory(rlmService)

tcpService = internet.TCPServer(port, factory)
tcpService.setServiceParent(application)
