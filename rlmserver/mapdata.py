
class Tile(object):
    """ A background tile on a map"""

    def __init__(self, name, back_r = 52, back_g = 148, back_b = 71,
                 fore_r = 255, fore_g = 255, fore_b = 255, char = " ",
                 blocks_movement = False, blocks_sight = False,
                 render_heightmap = True):
        self.name = name
        self.back_r = back_r
        self.back_g = back_g
        self.back_b = back_b
        self.fore_r = fore_r
        self.fore_g = fore_g
        self.fore_b = fore_b
        self.char = char
        self.blocks_movement = blocks_movement
        self.blocks_sight = blocks_sight
        self.render_heightmap = render_heightmap

    def back(self):
        return (self.back_r, self.back_g, self.back_b)



# single walls
CHAR_HLINE = chr(196)
CHAR_VLINE = chr(179)
CHAR_NE = chr(191)
CHAR_NW = chr(218)
CHAR_SE = chr(217)
CHAR_SW = chr(192)
CHAR_TEEW = chr(180)
CHAR_TEEE = chr(195)
CHAR_TEEN = chr(193)
CHAR_TEES = chr(194)
CHAR_CROSS = chr(197)
# double walls
CHAR_DHLINE = chr(205)
CHAR_DVLINE = chr(186)
CHAR_DNE = chr(187)
CHAR_DNW = chr(201)
CHAR_DSE = chr(188)
CHAR_DSW = chr(200)
CHAR_DTEEW = chr(185)
CHAR_DTEEE = chr(204)
CHAR_DTEEN = chr(202)
CHAR_DTEES = chr(203)
CHAR_DCROSS = chr(206)
# blocks
CHAR_BLOCK1 = chr(176)
CHAR_BLOCK2 = chr(177)
CHAR_BLOCK3 = chr(178)
# arrows
CHAR_ARROW_N = chr(24)
CHAR_ARROW_S = chr(25)
CHAR_ARROW_E = chr(26)
CHAR_ARROW_W = chr(27)
# arrows without tail
CHAR_ARROW2_N = chr(30)
CHAR_ARROW2_S = chr(31)
CHAR_ARROW2_E = chr(16)
CHAR_ARROW2_W = chr(17)
# double arrows
CHAR_DARROW_H = chr(29)
CHAR_DARROW_V = chr(18)
# GUI stuff
CHAR_CHECKBOX_UNSET = chr(224)
CHAR_CHECKBOX_SET = chr(225)
CHAR_RADIO_UNSET = chr(9)
CHAR_RADIO_SET = chr(10)
# sub-pixel resolution kit
CHAR_SUBP_NW = chr(226)
CHAR_SUBP_NE = chr(227)
CHAR_SUBP_N = chr(228)
CHAR_SUBP_SE = chr(229)
CHAR_SUBP_DIAG = chr(230)
CHAR_SUBP_E = chr(231)
CHAR_SUBP_SW = chr(232)

tiles = {
    # Admin Stuff
    0: Tile("admincrete", 70, 70, 70, 85, 85, 85, "+"),
    
    # Ocean - 100>
    100: Tile("ocean_1" , 0, 0,  100, 0, 0, 110, "     ~-"),
    101: Tile("ocean_2" , 0, 0,  130, 0, 0, 110, "     ~-"),
    102: Tile("ocean_3" , 0, 0,  160, 0, 0, 110, "     ~-"),
    103: Tile("ocean_4" , 0, 0,  190, 0, 0, 110, "     ~-"),
    104: Tile("ocean_5" , 0, 16, 220, 0, 0, 110, "     ~-"),
    105: Tile("ocean_6" , 0, 32, 220, 0, 0, 110, "     ~-"),
    106: Tile("ocean_7" , 0, 48, 220, 0, 0, 110, "     ~-"),
    107: Tile("ocean_8" , 0, 64, 220, 0, 0, 110, "     ~-"),
    108: Tile("ocean_9" , 0, 80, 220, 0, 0, 110, "     ~-"),
    109: Tile("ocean_10", 0, 96, 220, 0, 0, 110, "     ~-"),

    # Sand - 200>
    200: Tile("sand_1", 196, 191, 150, 180, 170, 120, CHAR_BLOCK1),
    201: Tile("sand_2", 207, 201, 150, 180, 170, 120, CHAR_BLOCK1),
    202: Tile("sand_3", 218, 212, 150, 180, 170, 120, CHAR_BLOCK1),
    203: Tile("sand_4", 229, 222, 150, 180, 170, 120, CHAR_BLOCK1),
    204: Tile("sand_5", 240, 233, 150, 180, 170, 120, CHAR_BLOCK1),

    # Grass - 500>
    500: Tile("grass_1",  39,  117, 0, 54, 205, 50, "      `,.:;"),
    501: Tile("grass_2",  43,  119, 0, 54, 205, 50, "      `,.:;"),
    502: Tile("grass_3",  47,  121, 0, 54, 205, 50, "      `,.:;"),
    503: Tile("grass_4",  52,  122, 0, 54, 205, 50, "      `,.:;"),
    504: Tile("grass_5",  61,  126, 0, 54, 205, 50, "      `,.:;"),
    505: Tile("grass_6",  70,  130, 0, 54, 205, 50, "      `,.:;"),
    506: Tile("grass_7",  80,  135, 2, 54, 205, 50, "      `,.:;"),
    507: Tile("grass_8",  88,  139, 2, 54, 205, 50, "      `,.:;"),
    508: Tile("grass_9",  96,  142, 3, 54, 205, 50, "      `,.:;"),
    509: Tile("grass_10",  106, 147, 4, 54, 205, 50, "      `,.:;"),
    510: Tile("grass_11",  121, 153, 5, 54, 205, 50, "      `,.:;"),
    511: Tile("grass_12",  133, 158, 6, 54, 205, 50, "      `,.:;"),
    512: Tile("grass_13",  145, 163, 7, 54, 205, 50, "      `,.:;"),
    513: Tile("grass_14",  148, 164, 12, 54, 205, 50, "      `,.:;"),
    514: Tile("grass_15",  151, 165, 17, 54, 205, 50, "      `,.:;"),
    515: Tile("grass_16",  154, 167, 22, 54, 205, 50, "      `,.:;"),
    516: Tile("grass_17",  158, 169, 29, 54, 205, 50, "      `,.:;"),
    517: Tile("grass_18",  163, 171, 36, 54, 205, 50, "      `,.:;"),
    518: Tile("grass_19",  167, 173, 42, 54, 205, 50, "      `,.:;"),
    519: Tile("grass_20",  180, 179, 63, 54, 205, 50, "      `,.:;"),
    520: Tile("grass_21",  198, 187, 91, 54, 205, 50, "      `,.:;"),
    521: Tile("grass_22", 186, 172, 83,  54, 205, 50, "      `,.:;"),
    522: Tile("grass_23", 174, 159, 76,  54, 205, 50, "      `,.:;"),
    523: Tile("grass_24", 164, 147, 69,  54, 205, 50, "      `,.:;"),
    524: Tile("grass_25", 156, 137, 64,  54, 205, 50, "      `,.:;"),
    525: Tile("grass_26", 151, 131, 61,  54, 205, 50, "      `,.:;"),

    # Snow 1000>:
    1000: Tile("snow_1", 205, 205, 255, 255, 255, 255, " "),
    1001: Tile("snow_2", 215, 215, 255, 255, 255, 255, " "),
    1002: Tile("snow_3", 225, 225, 255, 255, 255, 255, " "),
    1003: Tile("snow_4", 235, 235, 255, 255, 255, 255, " "),
    1004: Tile("snow_5", 245, 245, 255, 255, 255, 255, " "),
    1005: Tile("snow_6", 255, 255, 255, 255, 255, 255, " "),

    # Old Stuff
    32: Tile("stone_cave_wall_1", 50, 50, 50, 30, 30, 30, "#", True, True),
    33: Tile("stone_cave_wall_2", 70, 70, 70, 50, 50, 50, "#", True, True),
    34: Tile("stone_cave_wall_3", 90, 90, 90, 70, 70, 70, "#", True, True),
    35: Tile("stone_cave_wall_4", 110, 110, 110, 90, 90, 90, "#", True, True),
    36: Tile("stone_cave_wall_5", 130, 130, 130, 110, 110, 110, "#", True, True),
    37: Tile("stone_cave_wall_6", 150, 150, 150, 130, 130, 130, "#", True, True),
    38: Tile("marble_1", 90, 90, 100, 80, 80, 80, "+"),
    39: Tile("marble_2", 110, 110, 120, 100, 100, 100, "+"),
    40: Tile("marble_3", 130, 130, 140, 120, 120, 120, "+"),
    41: Tile("marble_4", 150, 150, 160, 140, 140, 140, "+"),
    42: Tile("mosaic_1", 150, 150, 160, 100, 100, 110, CHAR_RADIO_UNSET),
    43: Tile("mosaic_2", 153, 44, 75, 201, 121, 144, CHAR_RADIO_UNSET),
    44: Tile("mosaic_3", 150, 150, 160, 140, 140, 140, CHAR_RADIO_UNSET),
    45: Tile("mosaic_4", 150, 150, 160, 140, 140, 140, CHAR_RADIO_UNSET),
    48: Tile("stone_wall_hline", 90, 90, 90, 110, 110, 110, CHAR_HLINE, True, True),
    49: Tile("stone_wall_vline", 90, 90, 90, 110, 110, 110, CHAR_VLINE, True, True),
    50: Tile("stone_wall_ne", 90, 90, 90, 110, 110, 110, CHAR_NE, True, True),
    51: Tile("stone_wall_nw", 90, 90, 90, 110, 110, 110, CHAR_NW, True, True),
    52: Tile("stone_wall_se", 90, 90, 90, 110, 110, 110, CHAR_SE, True, True),
    53: Tile("stone_wall_sw", 90, 90, 90, 110, 110, 110, CHAR_SW, True, True),
    54: Tile("stone_wall_teew", 90, 90, 90, 110, 110, 110, CHAR_TEEW, True, True),
    55: Tile("stone_wall_teee", 90, 90, 90, 110, 110, 110, CHAR_TEEE, True, True),
    56: Tile("stone_wall_teen", 90, 90, 90, 110, 110, 110, CHAR_TEEN, True, True),
    57: Tile("stone_wall_tees", 90, 90, 90, 110, 110, 110, CHAR_TEES, True, True),
    58: Tile("stone_wall_cross", 90, 90, 90, 110, 110, 110, CHAR_CROSS, True, True),
}
