import random
import string
import os
import math
from datetime import datetime

import numpy

from twisted.internet.task import LoopingCall
from twisted.internet import reactor, defer
from twisted.python import log

import libtcod.libtcodpy as libtcod

from rlmserver.mapdata import tiles
from chunk import Chunk
from gui import Tooltip
from config import read_configuration
from clientfactory import RLMClientFactory


def distance(x1, y1, x2, y2):
    return math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )

class Entity(object):
    
    def __init__(self, username, x, y):
        self.username = username
        self.x = x
        self.y = y

class RLMClient:
    """ Main Game Class """

    # Server connection states
    (CON_STATE_CONNECTED, CON_STATE_DISCONNECTED) = range(2)

    # Number of ticks since game start
    ticks = 0

    # Framerate
    fps = 0

    # Networking
    server_host = ""
    server_port = 0
    factory = None

    # Server vars
    con_state = None  # Connection State

    def __init__(self):
        # Grab the configuration
        self.config = read_configuration()

        #self.username = self.config.get('player', 'name')
        self.username = ''.join(random.choice(string.ascii_uppercase) for x in
                                range(8))
        self.fps = self.config.getint('game', 'framerate')
        self.fps = 30
        self.server_host = self.config.get('server', 'host')
        self.server_port = self.config.getint('server', 'port')

        # Zone stuff
        self.zone_map = dict()
        self.in_zone = False
        self.chunks = dict()

        self.entities = dict()

        # size of the screen, in tiles
        self.SCR_W = 110 
        self.SCR_H = 70

        # Size of the map
        self.MAP_W = 80
        self.MAP_H = 50
        self.HALF_W = self.MAP_W / 2
        self.HALF_H = self.MAP_H / 2

        # Position of map on screen
        self.MAP_X = 1
        self.MAP_Y = 1

        self.TOOLTIP_DELAY = 0.3

        #initialize libtcod
        libtcod.console_set_custom_font(os.path.join('libtcod',
                                                     'fonts',
                                                     'dejavu10x10_gs_tc.png'),
                                        libtcod.FONT_TYPE_GREYSCALE | 
                                        libtcod.FONT_LAYOUT_TCOD)

        libtcod.console_init_root(self.SCR_W, self.SCR_H, 'RLMMO TEST', False)
        self.map = libtcod.console_new(self.MAP_W, self.MAP_H)
        self.ui_borders = libtcod.console_new(self.SCR_W, self.SCR_H)

        self.fov_map = libtcod.map_new(self.MAP_W, self.MAP_H)

        self.credits_end = False

        self.screen_b_r  = numpy.zeros((self.MAP_H, self.MAP_W), dtype="uint8")
        self.screen_b_b  = numpy.zeros((self.MAP_H, self.MAP_W), dtype="uint8")
        self.screen_b_g  = numpy.zeros((self.MAP_H, self.MAP_W), dtype="uint8")
        self.screen_f_r  = numpy.zeros((self.MAP_H, self.MAP_W), dtype="uint8")
        self.screen_f_b  = numpy.zeros((self.MAP_H, self.MAP_W), dtype="uint8")
        self.screen_f_g  = numpy.zeros((self.MAP_H, self.MAP_W), dtype="uint8")
        self.screen_char = numpy.zeros((self.MAP_H, self.MAP_W), dtype="uint8")
        self.screen_fov  = numpy.zeros((self.MAP_H, self.MAP_W), dtype="float")
        self.screen_bm   = numpy.zeros((self.MAP_H, self.MAP_W), dtype="bool")

        self.tick_times = []  # used for calculating average frame rate

        self.pause_time = 0.0  # Used to pause the player's actions after an action
        self.move_speed = 100.0  # How many cells per second the player moves

        self.tooltips = dict()

        self.mouse_last_cell = (0,0) # The position of the mouse
        self.mouse_last_cell_time = 0.0  # The time that the mouse has been in this positon

        self.fov_r = 100  # FOV Radius
        self.fov_algo = 0

        self.mounted = True

    def start(self, chaff):
        print self.username

        #self.splash_screen()

        # Render the main UI
        libtcod.console_set_default_foreground(self.map,libtcod.yellow)
        libtcod.console_set_default_background(self.map,libtcod.black)
        libtcod.console_set_alignment(self.map, libtcod.CENTER)
        libtcod.console_print(self.map, self.HALF_W, self.HALF_H, "Loading...")
        # Border around the map
        libtcod.console_set_default_foreground(self.ui_borders,libtcod.white)
        libtcod.console_set_default_background(self.ui_borders,libtcod.light_grey)
        libtcod.console_set_alignment(self.ui_borders, libtcod.LEFT)
        libtcod.console_print_frame(self.ui_borders,0,0,self.MAP_W+2,self.MAP_H+2)
        libtcod.console_print_frame(self.ui_borders,0,self.MAP_H+1,self.MAP_W+2,self.SCR_H-self.MAP_H-1)
        libtcod.console_print_frame(self.ui_borders,self.MAP_W+1,0,self.SCR_W-self.MAP_W-1, self.SCR_H)
        libtcod.console_put_char(self.ui_borders,0,self.MAP_H+1,chr(libtcod.CHAR_TEEE))
        libtcod.console_put_char(self.ui_borders,self.MAP_W+1,0,chr(libtcod.CHAR_TEES))
        libtcod.console_put_char(self.ui_borders,self.MAP_W+1,self.MAP_H+1,chr(libtcod.CHAR_TEEW))
        libtcod.console_put_char(self.ui_borders,self.MAP_W+1,self.SCR_H-1,chr(libtcod.CHAR_TEEN))
        #for x in range(1,self.MAP_W + 1):
        #    libtcod.console_print(self.ui_borders,
        #                          x,
        #                          0,
        #                          chr(196)
        #                          )
            

        libtcod.console_blit(self.map, 0, 0, 0, 0, 0, 1, 1)
        libtcod.console_blit(self.ui_borders, 0, 0, 0, 0, 0, 0, 0)

        print "Connecting to %s on port %d" % (self.server_host,
            self.server_port)
        self.connect_to_server()
        
        # Start the game tick loop using twisted reactor.
        self.tick = LoopingCall(self.game_tick)
        self.tick.start(1.0 / self.fps)


    def handle_input(self):
        
        mouse = libtcod.Mouse()
        key = libtcod.Key()
        ev = libtcod.sys_check_for_event(libtcod.EVENT_MOUSE|libtcod.EVENT_KEY_PRESS, key, mouse)

        if self.mouse_last_cell == (mouse.cx, mouse.cy):
            self.mouse_last_cell_time += 1.0 / self.fps
        else:
            self.mouse_last_cell_time = 0.0
            self.tooltips.clear()
        self.mouse_last_cell = (mouse.cx, mouse.cy)

        # Tooltips
        if self.mouse_last_cell_time > self.TOOLTIP_DELAY:
            if not self.tooltips.has_key((mouse.cx, mouse.cy)):
                # Disable this for now
                pass
                #self.tooltips[(mouse.cx, mouse.cy)] = Tooltip(mouse.cx, mouse.cy, title="2 sec")
        
        # Mouse stuff
        if mouse.rbutton:
            if 0 < mouse.cx < self.MAP_W + 1 and 0 < mouse.cy < self.MAP_H + 1:
                mx, my = self.scr_to_map_coords(mouse.cx, mouse.cy)
                self.move_player(mx, my)
        
        if mouse.lbutton_pressed:
            self.fov_r += 1
            if self.fov_r > 30: self.fov_r = 2
            print "FOV Radius: {}".format(self.fov_r)

        # KB Stuff
        if key.vk == libtcod.KEY_ESCAPE:
            self.tick.stop()
            reactor.stop()
            return
        if key.vk == libtcod.KEY_UP or key.c == ord("w"):
            self.move_player(self.player_x, self.player_y - 1)
            return
        if key.vk == libtcod.KEY_DOWN or key.c == ord("s"):
            self.move_player(self.player_x, self.player_y + 1)
            return
        if key.vk == libtcod.KEY_LEFT or key.c == ord("a"):
            self.move_player(self.player_x - 1, self.player_y)
            return
        if key.vk == libtcod.KEY_RIGHT or key.c == ord("d"):
            self.move_player(self.player_x + 1, self.player_y)
            return


    def scr_to_map_coords(self, scr_x, scr_y):
        map_x = self.mtl_x + scr_x - self.MAP_X
        map_y = self.mtl_y + scr_y - self.MAP_Y
        return(map_x, map_y)

    def map_to_console_coords(self, map_x, map_y):
        con_x = map_x - self.mtl_x
        con_y = map_y - self.mtl_y
        return(con_x, con_y)

    def console_to_map_coords(self, con_x, con_y):
        map_x = con_x + self.mtl_x
        map_y = con_y + self.mtl_y
        return(map_x, map_y)

    def move_player(self, x, y):
        # Check if cell is blocked
        bm = True
        if (x/64, y/64) in self.chunks:
            bm = self.chunks[(x/64,y/64)].blocks_movement[y%64,x%64]
        if not bm:
            self.move_destination = (x, y)
        #    self.player_x = new_x
        #    self.player_y = new_y
        #    self.protocol.write_packet("location",
        #                               x = self.player_x,
        #                               y = self.player_y)


    def player_teleport(self, username, x, y):
        if self.entities.has_key(username):
            self.entities[username].x = x
            self.entities[username].y = y
            log.msg("Teleport Entity {} - {},{}".format(username, x, y))
        else:
            log.msg("Creating new entity: {}".format(username))
            self.entities[username] = Entity(username, x, y)

    
    def player_destroy(self, username):
        if self.entities.has_key(username):
            del self.entities[username]


    def receive_chunk(self, x, y, data):
        """ Receives chunk data from the server and sticks it in memory """

        self.chunks[(x,y)] = Chunk(x, y, data)

    def game_tick(self):
        # Debug framerate
        self.ticks += 1

        avg_tick_time = "NA"
        if len(self.tick_times) == 100:
            vals = sorted(self.tick_times)
            avg_tick_time = vals[50]

        dt = datetime.now()
        time_start = dt.microsecond
        
        self.handle_input()

        if self.in_zone:  # We have been send a newzone packet

            # Decay pause time
            time_per_tick = 1.0 / self.fps
            self.pause_time -= time_per_tick
            if self.pause_time <= 0.0: self.pause_time = 0.0

            # Calculate map top left coordinates
            self.mtl_x = self.player_x - self.HALF_W
            self.mtl_y = self.player_y - self.HALF_H

            # Which screen coord are we up to?
            screen_x = 0
            screen_y = 0

            # Start off black
            self.screen_b_r[:] = 0
            self.screen_b_g[:] = 0
            self.screen_b_b[:] = 0
            self.screen_f_r[:] = 100
            self.screen_f_g[:] = 0
            self.screen_f_b[:] = 0
            self.screen_char[:] = ord("?")
            self.screen_bm[:] = True

            while screen_y < self.MAP_H:
                # Which chunk are we in?
                c_x = (self.mtl_x + screen_x) / 64
                c_y = (self.mtl_y + screen_y) / 64

                # What internal coords do we start slicing from?
                int_x = (self.mtl_x + screen_x) % 64
                int_y = (self.mtl_y + screen_y) % 64

                # What internal coords do we stop slicing to?
                int_x_2 = int_x + min(self.MAP_W - screen_x, 64 - int_x)
                int_y_2 = int_y + min(self.MAP_H - screen_y, 64 - int_y)

                # Size of the slice
                size_x = int_x_2 - int_x
                size_y = int_y_2 - int_y

                screen_x_2 = screen_x + size_x
                screen_y_2 = screen_y + size_y

                # copy the cells
                if (c_x, c_y) in self.chunks:
                    self.screen_b_r[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = self.chunks[(c_x,c_y)].b_r[int_y:int_y_2,
                                                                                      int_x:int_x_2]
                    self.screen_b_g[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = self.chunks[(c_x,c_y)].b_g[int_y:int_y_2,
                                                                                      int_x:int_x_2]
                    self.screen_b_b[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = self.chunks[(c_x,c_y)].b_b[int_y:int_y_2,
                                                                                      int_x:int_x_2]
                    self.screen_f_r[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = self.chunks[(c_x,c_y)].f_r[int_y:int_y_2,
                                                                                      int_x:int_x_2]
                    self.screen_f_g[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = self.chunks[(c_x,c_y)].f_g[int_y:int_y_2,
                                                                                      int_x:int_x_2]
                    self.screen_f_b[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = self.chunks[(c_x,c_y)].f_b[int_y:int_y_2,
                                                                                      int_x:int_x_2]
                    self.screen_char[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = self.chunks[(c_x,c_y)].char[int_y:int_y_2,
                                                                                       int_x:int_x_2]
                    self.screen_bm[screen_y:screen_y_2,
                                    screen_x:screen_x_2] = 1 - self.chunks[(c_x,c_y)].blocks_movement[int_y:int_y_2,
                                                                                                  int_x:int_x_2]

                # Now where are we up to?
                screen_x = screen_x + size_x
                if screen_x == self.MAP_W:
                    screen_x = 0
                    screen_y = screen_y + size_y

            # Create the FOV overlay
            #for y in xrange(self.MAP_H):
            #    for x in xrange(self.MAP_W):
            #        blocked = self.screen_bm[y, x]
            #        libtcod.map_set_properties(self.fov_map,
            #                                   x,
            #                                   y,
            #                                   blocked,
            #                                   blocked)
            libtcod.map_fill_properties(self.fov_map, self.screen_bm, self.screen_bm)

            libtcod.map_compute_fov(self.fov_map,
                                    self.player_x - self.mtl_x,
                                    self.player_y - self.mtl_y,
                                    self.fov_r,
                                    True,
                                    self.fov_algo)

            for y in xrange(self.MAP_H):
                for x in xrange(self.MAP_W):
                    zx, zy = self.console_to_map_coords(x,y)
                    c = (zx/64, zy/64)
                    ix = zx % 64
                    iy = zy % 64
                    oof = 0.7  # out of fov value
                    if libtcod.map_is_in_fov(self.fov_map, x, y):
                        # This cell is visible
                        d = distance(self.player_x, self.player_y, zx, zy)  # Distance between cell and player
                        v = min(1.0,(oof + (self.fov_r - d) / (self.fov_r / 4.0) * (1 - oof)))
                        if v < oof: v = oof
                        self.screen_fov[y,x] = v
                        # Set it to explored
                        if c in self.chunks:
                            self.chunks[c].explored[ix,iy] = True
                    elif c in self.chunks and self.chunks[c].explored[ix,iy]:
                        self.screen_fov[y,x] = oof
                    else:
                        self.screen_fov[y,x] = 1.0
                        self.screen_b_r[y,x] = 0
                        self.screen_b_g[y,x] = 0
                        self.screen_b_b[y,x] = 0
                        self.screen_f_r[y,x] = 25
                        self.screen_f_g[y,x] = 20
                        self.screen_f_b[y,x] = 35
                        self.screen_char[y,x] = libtcod.CHAR_BLOCK1

            #self.screen_fov[:] = 0.8  # Set the entire screen to some opacity
            #cx = self.player_x - self.mtl_x
            #cy = self.player_y - self.mtl_y
            #y, x = numpy.ogrid[-fov_r: fov_r, -fov_r: fov_r]
            #index = x**2 + y**2 <= fov_r**2-1
            #r = int(len(index) / 2)
            #self.screen_fov[cy-r:cy+r, cx-r:cx+r][index] = 1

            # Apply the fov opacity
            self.screen_b_r *= self.screen_fov
            self.screen_b_g *= self.screen_fov
            self.screen_b_b *= self.screen_fov
            self.screen_f_r *= self.screen_fov
            self.screen_f_g *= self.screen_fov
            self.screen_f_b *= self.screen_fov

            # Fill libtcod
            libtcod.console_fill_background(self.map,
                                            self.screen_b_r,
                                            self.screen_b_g,
                                            self.screen_b_b)
            libtcod.console_fill_foreground(self.map,
                                            self.screen_f_r,
                                            self.screen_f_g,
                                            self.screen_f_b)
            libtcod.console_fill_char(self.map, self.screen_char)

            # Render other players
            for player in self.entities.itervalues():
                psx = player.x - self.mtl_x
                psy = player.y - self.mtl_y
                if 0 < psx < self.MAP_W and 0 < psy < self.MAP_H and libtcod.map_is_in_fov(self.fov_map, psx, psy):
                    name_y = psy - 1
                    if name_y < 0: name_y = 2
                    libtcod.console_set_default_foreground(self.map,libtcod.grey)
                    libtcod.console_set_alignment(self.map, libtcod.CENTER)
                    libtcod.console_print(self.map, psx, name_y, player.username)
                    libtcod.console_set_default_foreground(self.map,libtcod.cyan)
                    libtcod.console_set_alignment(self.map, libtcod.CENTER)
                    libtcod.console_print(self.map, psx, psy, "@")

            # Render coords in top right
            libtcod.console_set_default_foreground(self.map, libtcod.cyan)
            libtcod.console_set_alignment(self.map, libtcod.RIGHT)
            libtcod.console_print(self.map, self.MAP_W - 2, 1,
                                  "Your coordinates: [{},{}]".format(self.player_x, self.player_y))
            # Render the player's symbol
            psx = self.player_x - self.mtl_x
            psy = self.player_y - self.mtl_y
            libtcod.console_set_default_foreground(self.map, libtcod.white)
            libtcod.console_set_alignment(self.map, libtcod.LEFT)
            libtcod.console_print(self.map, psx, psy, "@")
            # Render the mount
            if self.mounted:
                msx = self.player_last_x - self.mtl_x
                msy = self.player_last_y - self.mtl_y
                libtcod.console_set_default_foreground(self.map, libtcod.sepia)
                libtcod.console_print(self.map, msx, msy, "M")

            # Render the median tick time
            if not avg_tick_time == "NA":
                libtcod.console_set_default_foreground(self.map, libtcod.white)
                libtcod.console_set_alignment(self.map, libtcod.RIGHT)
                libtcod.console_print(self.map,
                                      self.MAP_W - 2,
                                      self.MAP_H - 2,
                                      "Avg time to render one frame: {}ms".format(round(avg_tick_time/1000.0,1)))

            # Do any movement
            if not (self.player_x, self.player_y) == self.move_destination and self.pause_time == 0:
                map_dest_x, map_dest_y = self.move_destination
                con_dest_x, con_dest_y = self.map_to_console_coords(map_dest_x, map_dest_y)
                root_x, root_y = self.map_to_console_coords(self.player_x, self.player_y)

                path = libtcod.path_new_using_map(self.fov_map, 1.41)
                libtcod.path_compute(path, root_x, root_y, con_dest_x, con_dest_y)
                
                
                if libtcod.path_size(path):
                    con_new_x, con_new_y = libtcod.path_get(path, 0)
                    new_x, new_y = self.console_to_map_coords(con_new_x, con_new_y)

                    # Did we move diagonally?
                    if new_x != self.player_x and new_y != self.player_y:
                        self.pause_time = 1 / (self.move_speed / 1.6)
                    else:
                        self.pause_time = 1 / self.move_speed
                    self.player_last_x = self.player_x  # Update our last position
                    self.player_last_y = self.player_y
                    self.player_x = new_x
                    self.player_y = new_y
                    self.protocol.write_packet("location",
                                               x = self.player_x,
                                               y = self.player_y)

        libtcod.console_blit(self.map, 0, 0, 0, 0, 0, 1, 1)
        
        for tip in self.tooltips.itervalues():
            tip.render()

        libtcod.console_flush()

        dt = datetime.now()
        time_stop = dt.microsecond
        self.tick_times.append(time_stop - time_start)
        self.tick_times = self.tick_times[-100:]



    def new_zone(self, name, spawn_x, spawn_y):
        """ Received a new zone packet from the server. spawn_x and spawn_y
        being the location of the player in the new zone. """

        self.zone_map = dict()  # Reset the zone map.
        self.zone_name = name
        self.player_x = spawn_x
        self.player_y = spawn_y
        self.player_last_x = spawn_x
        self.player_last_y = spawn_y + 1
        self.move_destination = (spawn_x, spawn_y)
        self.in_zone = True



    def splash_screen(self):
        self.buffer = libtcod.ConsoleBuffer(self.SCR_W, self.SCR_H) 
        while not self.credits_end:
            #escape key to exit
            key = libtcod.console_check_for_keypress()
            if key.vk == libtcod.KEY_ESCAPE:
                break 

            im = Image.open("rlmclient/rlmmo.png")
            pix_x, pix_y = im.size
            pix = im.load()
            logo_x = int((self.SCR_W - pix_x) / 2)
            logo_y = 5
            for y in xrange(pix_y):
                for x in xrange(pix_x):
                    r, g, b = pix[x,y]
                    self.buffer.set_back(logo_x + x, logo_y + y, r, g, b)

            #draw the buffer's contents to a console
            self.buffer.blit(0)

            # render credits
            self.credits_end = libtcod.console_credits_render(62, 44, 0)

            libtcod.console_flush()

        for y in xrange(self.SCR_H):
            for x in xrange(self.SCR_W):
                self.buffer.set_back(x,y,0,0,0)
        self.buffer.blit(0)
        libtcod.console_flush()

    def connect_to_server(self):
        self.factory = RLMClientFactory()
        self.factory.rlmclient = self
        reactor.connectTCP(self.server_host, self.server_port, self.factory)
    
    def con_state_connected(self):
        self.con_state = self.CON_STATE_CONNECTED

        # Send a login packet
        self.protocol.loginrequest()

    def con_state_disconnected(self):
        self.con_state = self.CON_STATE_DISCONNECTED
