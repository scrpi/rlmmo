import libtcod.libtcodpy as libtcod

class Tooltip(object):

    def __init__(self, x, y, w = 10, h = 10, title = "Tooltip"):
        print x,y,w,h
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.title = title
        self.console = libtcod.console_new(self.w, self.h)

    def render(self):
        libtcod.console_set_default_foreground(self.console,libtcod.white)
        libtcod.console_set_default_background(self.console,libtcod.light_grey)
        libtcod.console_set_alignment(self.console, libtcod.CENTER)
        libtcod.console_print(self.console, int(self.w / 2), 0, self.title)
        libtcod.console_blit(self.console, 0, 0, self.w, self.h, 0, self.x, self.y, 1.0, 0.6)
