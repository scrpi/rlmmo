from ConfigParser import SafeConfigParser, NoSectionError, NoOptionError

class RLMConfigParser(SafeConfigParser):

    """ Extended ConfigParser """

    # define extensions here.


def read_configuration():
    """ Parses configuration from file and returns RLMConfigParser object. """
    configuration = RLMConfigParser()

    # for now we are defaulting to ./conf/rlmclient.ini 
    configuration.read('conf/rlmclient.ini')

    return configuration
