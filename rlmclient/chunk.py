import random

from construct import Struct, Array, UBInt8
from construct import UBInt16, Container, Bit

from rlmserver.mapdata import tiles

try:
    import numpypy
except:
    pass
import numpy

class Chunk(object):
    """ Represents a region of cells in memory """

    def __init__(self, x, y, data):

        self.x = x
        self.y = y

        packet_struct = Struct('chunk',
                           Array(64, Array(64, UBInt16('tiles'))),
                           Array(64, Array(64, Bit('blocks_movement'))),
                           Array(64, Array(64, UBInt8('biome'))),
                           Array(64, Array(64, UBInt8('height'))),
                           )

        c = packet_struct.parse(data)

        self.tiles = numpy.array(c.tiles, dtype="uint16")
        self.blocks_movement = numpy.array(c.blocks_movement, dtype="bool")

        self.b_r = numpy.zeros((64, 64), dtype="uint8")
        self.b_g = numpy.zeros((64, 64), dtype="uint8")
        self.b_b = numpy.zeros((64, 64), dtype="uint8")
        self.f_r = numpy.zeros((64, 64), dtype="uint8")
        self.f_g = numpy.zeros((64, 64), dtype="uint8")
        self.f_b = numpy.zeros((64, 64), dtype="uint8")
        self.char = numpy.zeros((64, 64), dtype="uint8")

        self.explored = numpy.zeros((64, 64), dtype="bool")

        self.do_render()

    def do_render(self):
        for x in xrange(64):
            for y in xrange(64):
                t = tiles[self.tiles[x,y]]
                self.b_r[x,y] = t.back_r
                self.b_g[x,y] = t.back_g
                self.b_b[x,y] = t.back_b
                self.f_r[x,y] = t.fore_r
                self.f_g[x,y] = t.fore_g
                self.f_b[x,y] = t.fore_b
                self.char[x,y] = ord(random.choice(t.char))
        self.b_r = numpy.flipud(numpy.rot90(self.b_r))
        self.b_g = numpy.flipud(numpy.rot90(self.b_g))
        self.b_b = numpy.flipud(numpy.rot90(self.b_b))
        self.f_r = numpy.flipud(numpy.rot90(self.f_r))
        self.f_g = numpy.flipud(numpy.rot90(self.f_g))
        self.f_b = numpy.flipud(numpy.rot90(self.f_b))
        self.char = numpy.flipud(numpy.rot90(self.char))
        self.blocks_movement = numpy.flipud(numpy.rot90(self.blocks_movement))
