from twisted.internet import reactor, protocol

from clientprotocol import RLMClientProtocol
       
class RLMClientFactory(protocol.ClientFactory):
    """
    Docs
    """
    def __init__(self):
        pass

    def buildProtocol(self, addr):
        self.p = RLMClientProtocol()
        self.p.factory = self
        self.p.rlmclient = self.rlmclient
        return self.p

    def clientConnectionLost(self, connector, reason):
        pass

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
        self.rlmclient.con_state_disconnected()
        reactor.stop()
