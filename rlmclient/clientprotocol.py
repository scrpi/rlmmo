from twisted.internet import reactor, protocol
from twisted.python import log

from rlmserver.packets import make_packet, parse_packets

class RLMClientProtocol(protocol.Protocol):

    """
    Docs
    """

    def __init__(self):
        self.buf = ""
        self.handlers = {
            20: self.newzone,
            21: self.sendcells,
            30: self.chunk,
            40: self.location,
            41: self.player_teleport,
            42: self.player_destroy,
            254: self.ping,
            255: self.error,
        }


    ##################################
    # Server > Client packet handlers

    def newzone(self, container):
        self.rlmclient.new_zone(container.name, container.x, container.y)

    def sendcells(self, container):
        """
        Handle a sendcells packet from the server.
        This is the server sending us a list of tuples containing Cell data
        which has then been pickled, and compressed.
        """
        _bytes = 0
        for i in container.celldata:
            _bytes += len(i)
            self.rlmclient.zone_map[(i.x, i.y)] = [i.tile,
                                                   i.blocks_movement,
                                                   i.hm,
                                                   ]

        log.msg('Received sendcells payload, length {} bytes'.format(_bytes))

    def chunk(self, container):
        """ Receives a chunk packet from the server """

        self.rlmclient.receive_chunk(container.x, container.y, container.data)

    def location(self, container):
        """ Handle a location update from the server """
        self.rlmclient.player_x = container.x
        self.rlmclient.player_y = container.y

    def player_teleport(self, container):
        if not container.username == self.rlmclient.username:
            self.rlmclient.player_teleport(container.username,
                                           container.x,
                                           container.y)

    def player_destroy(self, container):
        self.rlmclient.player_destroy(container.username)

    def error(self, container):
        log.msg('Received error packet (%d): %s' % (container.type,
                container.message))
        self.transport.loseConnection()
        reactor.stop()

    def ping(self, container):
        """
        Handle a ping container
        """
        self.write_packet('ping', pid=container.pid)


    ##################################
    # Connection Methods

    def loginrequest(self):
        n = self.rlmclient.username
        t = "randomstring"
        p = 0
        self.write_packet('loginrequest', username = n, token = t,
                         protocol_version = p)

    ##################################
    # Connection Methods

    def connectionMade(self):
        print("Connection Established")
        self.rlmclient.protocol = self
        self.rlmclient.con_state_connected()

    def connectionLost(self, reason):
        pass

    def write_packet(self, header, **payload):
        """
        Send a packet to the client.

        """
        self.transport.write(make_packet(header, **payload))

    def dataReceived(self, data):
        self.buf += data
        
        packets, self.buf = parse_packets(self.buf)

        for header, payload in packets:
            if header in self.handlers	:
                self.handlers[header](payload)
            else:
                print("Didn't handle parseable packet %d!" % header)
                print(payload)


