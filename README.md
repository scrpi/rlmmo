## About

'rlmmo' i.e. "Rogue Like MMO" is my attempt at writing a massively multiplayer roguelike game in python. The code is currently very very alpha.

A lot of the network code is inspired by MostAwesomeDude's bravo (minecraft server) - https://github.com/MostAwesomeDude/bravo

## Dependencies

* twisted
* construct
* ConfigParser
* libtcod
* numpy

### Running RLMMO

### Server:
* For running the server in non daemon mode:


        ./nod.sh 

* Starting in Daemon mode:

        ./restart.sh

* Stopping the Daemon:

        ./kill.sh

### Client

        python ./client.py
