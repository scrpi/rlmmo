import sys
import os

import libtcod.libtcodpy as libtcod

from rlmserver.chunk import Chunk
from rlmserver.mapdata import tiles

class ZoneEditor(object):

    def __init__(self, zone_id):
        self.zone_id = int(zone_id)

    def start(self):
        #size of the screen, in tiles
        self.SCREEN_W = 130 
        self.SCREEN_H = 80
        self.HALF_W = self.SCREEN_W / 2
        self.HALF_H = self.SCREEN_H / 2

        #initialize libtcod
        libtcod.console_set_custom_font(os.path.join('libtcod',
                                                     'fonts',
                                                     'arial10x10.png'),
                                        libtcod.FONT_TYPE_GREYSCALE | 
                                        libtcod.FONT_LAYOUT_TCOD)

        libtcod.console_init_root(self.SCREEN_W, self.SCREEN_H, 'RLMMO TEST', 
                                  False)
        self.con = libtcod.console_new(self.SCREEN_W, self.SCREEN_H)
        self.pnl = libtcod.console_new(self.SCREEN_W, 10)

        libtcod.console_disable_keyboard_repeat()

        #libtcod.sys_set_fps(2)

        self.credits_end = False
        #create a buffer the same size as the screen
        self.buffer = libtcod.ConsoleBuffer(self.SCREEN_W, self.SCREEN_H)

        # Check if this zone exists, if not lets create it
        self.zone_dir = "data/chunks/{}".format(self.zone_id)
        if not os.path.exists(self.zone_dir):
            os.makedirs(self.zone_dir)

        # Load chunks into memory
        self.chunks = dict()
        dir_list = os.listdir(self.zone_dir)
        for fn in dir_list:
            if fn[-3:] == 'bin':
                # We can assume that since this ends in bin this is a chunk
                chaff, x, y = fn[:-4].split('_', 3)
                x = int(x)
                y = int(y)
                self.chunks[(x,y)] = Chunk(self.zone_id, x, y)
                print "Loaded chunk at coord: {},{}".format(x, y)

        self.centered_x = 0  # map coordinate that the screen is centered on
        self.centered_y = 0

        self.select_1 = False  # Select box
        self.select_2 = False

        self.command_mode = False

        self.main_loop()

    def main_loop(self):
        while not libtcod.console_is_window_closed():

            self.render_screen()

            action = self.handle_input()

            if self.select_1:
                self.select_2 = (self.centered_x, self.centered_y)

            if action == "exit":
                break


    def render_screen(self):

        map_top_left_x = self.centered_x - self.HALF_W
        map_top_left_y = self.centered_y - self.HALF_H

        center_tile = False

        for x in xrange(self.SCREEN_W):
            for y in xrange(self.SCREEN_H):
                pass
                map_x = map_top_left_x + x
                map_y = map_top_left_y + y
                chunk_coord = self.chunk_coord_from_zone_coord((map_x,
                                                                map_y))
                if self.chunks.has_key(chunk_coord):
                    c = self.chunks[chunk_coord]
                    internal_x = map_x % 64
                    internal_y = map_y % 64
                    t = tiles[c.tiles[internal_x, internal_y]]
                    if (x, y) == (self.HALF_W, self.HALF_H):
                        center_tile = t
                    if t.render_heightmap:
                        hm_val = c.hm[internal_x, internal_y]
                        mod = hm_val / 8 - 16
                        mod = int(mod * 0.7)
                        b_r = max(0, min(t.back_r + mod, 255))
                        b_g = max(0, min(t.back_g + mod, 255))
                        b_b = max(0, min(t.back_b + mod, 255))
                        f_r = max(0, min(t.fore_r + mod, 255))
                        f_g = max(0, min(t.fore_g + mod, 255))
                        f_b = max(0, min(t.fore_b + mod, 255))
                    else:
                        b_r = t.back_r
                        b_g = t.back_g
                        b_b = t.back_b
                        f_r = t.fore_r
                        f_g = t.fore_g
                        f_b = t.fore_b
                    self.buffer.set(x, y, b_r, b_g, b_b, f_r, f_g, f_b, t.char)

        # Render the center tile
        def ltn(val = 0):
            val += 35
            return max(0, min(val, 255))
        if center_tile:
            self.buffer.set(self.HALF_W, self.HALF_H, ltn(center_tile.back_r),
                            ltn(center_tile.back_g), ltn(center_tile.back_b),
                            ltn(center_tile.fore_r), ltn(center_tile.fore_g),
                            ltn(center_tile.fore_b), center_tile.char)
        else:
            self.buffer.set(self.HALF_W, self.HALF_H,35,35,35,35,35,35," ")

        # Render a selection box
        #if self.select_1 and self.select_2:
        #    x_1, y_1 = self.map2screen(self.select_1)
        #    x_2, y_2 = self.map2screen(self.select_2)
        #    for x in xrange(x_1, x_2):
        #        for y in xrange(y_1, y_2):
        #            br, bg, bb, fr, fg, fb, c = self.get_from_buffer(x,y)
        #            self.buffer.set(x,y,ltn(br),ltn(bg),ltn(bb),ltn(fr),
        #                            ltn(fg),ltn(fb),c)

        self.buffer.blit(self.con)

        libtcod.console_set_default_foreground(self.con, libtcod.white)
        libtcod.console_set_alignment(self.con, libtcod.RIGHT)
        libtcod.console_print(self.con, self.SCREEN_W - 2, 1,
                              "Centered on: [{},{}]".format(self.centered_x,
                                                            self.centered_y))

        # Render the main console
        libtcod.console_blit(self.con, 0, 0, self.SCREEN_W, self.SCREEN_H, 0,
                             0, 0)


        libtcod.console_flush()

        self.buffer.clear()

    def get_from_buffer(self, x, y):
        i = self.SCREEN_W * y + x
        r = (self.buffer.back_r[i],
             self.buffer.back_g[i],
             self.buffer.back_b[i],
             self.buffer.fore_r[i],
             self.buffer.fore_g[i],
             self.buffer.fore_b[i],
             chr(self.buffer.char[i])
            )
        return r

    def map2screen(self, coord):
        map_x, map_y = coord
        scr_x = (map_x - self.centered_x) + self.HALF_W
        scr_y = (map_y - self.centered_y) + self.HALF_H
        return (scr_x, scr_y)

    def chunk_coord_from_zone_coord(self, coord):
        x, y = coord
        return (x / 64, y / 64)

    def render_panel(self):
        # Render the input panel
        libtcod.console_clear(self.pnl)
        libtcod.console_set_default_foreground(self.pnl,
                                               libtcod.white)
        libtcod.console_print(self.pnl, 1, 9, self.command_input)
        libtcod.console_blit(self.pnl, 0, 0, self.SCREEN_W, 10, 0, 0,
                             self.SCREEN_H - 10, 1, 0.5) 
        

    def handle_input(self):

        if self.command_mode:
            self.render_panel()
            key = libtcod.console_wait_for_keypress(True)
            while key.vk != libtcod.KEY_ENTER and key.vk != libtcod.KEY_ESCAPE:
                letter = chr(key.c)
                self.command_input = self.command_input + letter
                self.render_panel()
                key = libtcod.console_wait_for_keypress(True)
            self.command_mode = False
            self.command_input = ""
        else:
            key = libtcod.Key()
            mouse = libtcod.Mouse()
            ev = libtcod.sys_check_for_event(libtcod.EVENT_MOUSE|EVENT_KEY_PRESS, key, mouse)

            if mouse.rbutton and (mouse.cx in range(1, self.SCREEN_W - 1) and 
                                  mouse.cy in range(1, self.SCREEN_H - 1)):
                self.centered_x -= mouse.dcx
                self.centered_y -= mouse.dcy

            if key.vk == libtcod.KEY_ENTER:
                self.command_mode = True
                self.command_input = ""
            elif key.vk == libtcod.KEY_ESCAPE:
                self.select_1 = False
                self.select_2 = False
            elif key.vk == libtcod.KEY_UP:
                self.centered_y -= 1
            elif key.vk == libtcod.KEY_DOWN:
                self.centered_y += 1
            elif key.vk == libtcod.KEY_LEFT:
                self.centered_x -= 1
            elif key.vk == libtcod.KEY_RIGHT:
                self.centered_x += 1
            elif key.vk == libtcod.KEY_SPACE:
                self.select_1 = (self.centered_x, self.centered_y)


def main():
    if len(sys.argv) <= 1:
        print "no zone id specified"
        return

    zone_id = sys.argv[1]

    ze = ZoneEditor(zone_id)
    ze.start()


if __name__ == '__main__':
    main()
