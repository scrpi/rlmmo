import os

from libtcod import libtcodpy as libtcod

from rlmserver.mapdata import tiles

font = os.path.join('libtcod', 'fonts', 'arial12x12.png')

libtcod.console_set_custom_font(font, libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)
libtcod.console_init_root(16, 16, 'Tiles', False)
con   = libtcod.console_new(16, 16)

buffer = libtcod.ConsoleBuffer(16, 16)

i = 0


for y in xrange(16):
    for x in xrange(16):
        if tiles.has_key(i):
            t = tiles[i]
        else:
            t = tiles[0]
        buffer.set(x, y, t.back_r, t.back_g, t.back_b,
                   t.fore_r, t.fore_g, t.fore_b, t.char)
        i += 1

buffer.blit(con)
libtcod.console_blit(con, 0, 0, 16, 16, 0, 0, 0)
libtcod.console_flush()

libtcod.console_wait_for_keypress(True)
