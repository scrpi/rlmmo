import sys
import os
import math

import libtcod.libtcodpy as libtcod

from construct import Struct, Array, UBInt8, Container, Bit

try:
    import numpypy
except:
    pass
import numpy

class Chunk(object):
    """ A chunk of cells."""

    CHUNK_DIR = "data/chunks"  # Move this to another spot?

    chunk_struct = Struct('chunk',
                          Array(64, Array(64, UBInt8('tiles'))),
                          Array(64, Array(64, Bit('blocks_movement'))),
                          )
    new_chunk_struct = Struct('chunk',
                          Array(64, Array(64, UBInt8('tiles'))),
                          Array(64, Array(64, Bit('blocks_movement'))),
                          Array(64, Array(64, UBInt8('hm'))),
                          )

    def __init__(self, zid, x, y):
        """
        :param int zid: zone id that this chunk belongs to.
        :param int x: X coordinate in chunk coords
        :param int y: Y coordinate in chunk coords
        """

        self.zid = int(zid)
        self.x = int(x)
        self.y = int(y)

        self.zone_dir = "{}/{}".format(self.CHUNK_DIR, self.zid)

        # File path prefix of where this chunk is stored on disk
        self.chunk_file = "{}/cnk_{}_{}.bin".format(self.zone_dir, self.x, self.y)

        # Set all file path locations
        #self.tiles_file = "{}.tiles.npy".format(self.file_prefix)

        self.load()

    def load(self):
        """ Loads a chunk from file if it exists """

        if os.path.exists(self.chunk_file):
            fp = open(self.chunk_file, "rb")  # Lets make nonblocking later
            data = fp.read()
            fp.close()

            c = self.chunk_struct.parse(data)
            self.tiles = numpy.array(c.tiles, dtype="uint8")

            self.blocks_movement = numpy.array(c.blocks_movement, dtype="bool")

            self.hm = numpy.zeros((64, 64), dtype="uint8")

            rnd=libtcod.random_new_from_seed(self.zid)
            noise=libtcod.noise_new(2,libtcod.NOISE_DEFAULT_HURST,libtcod.NOISE_DEFAULT_LACUNARITY,rnd)
            # function building the heightmap
            def buildMap(hm) :
                libtcod.heightmap_add_fbm(hm,noise,3,3,0,0,6,0.5,1)
                libtcod.heightmap_normalize(hm,0,1)
            # test code to print the heightmap
            hm=libtcod.heightmap_new(64, 64)
            buildMap(hm)
            for x in range(64) :
                for y in range(64) :
                    z = libtcod.heightmap_get_value(hm,x,y)
                    val=int(z*255) & 0xFF
                    self.hm[x,y] = val

    def save(self):
        """ Saves a chunk to file """

        # Check that the zone_dir exists, if not create.
        if not os.path.exists(self.zone_dir):
            os.makedirs(self.zone_dir)

        c = Container(tiles = self.tiles.tolist(),
                      blocks_movement = self.blocks_movement.tolist(),
                      hm = self.hm.tolist(),
                      )

        data = self.new_chunk_struct.build(c)

        fp = open(self.chunk_file, "wb")  # Lets make this non-blocking later.
        fp.write(data)
        fp.close()


def main():
    if len(sys.argv) <= 1:
        print "no zone id specified"
        return

    zone_id = sys.argv[1]

    # Check if this zone exists, if not lets create it
    zone_dir = "data/chunks/{}".format(zone_id)
    if not os.path.exists(zone_dir):
        print "Could not find zone at {}".format(zone_dir)
        return

    # Load chunks into memory
    chunks = dict()
    dir_list = os.listdir(zone_dir)
    for fn in dir_list:
        if fn[-3:] == 'bin':
            # We can assume that since this ends in bin this is a chunk
            chaff, x, y = fn[:-4].split('_', 3)
            x = int(x)
            y = int(y)
            c = Chunk(zone_id, x, y)
            c.save()
            print "Converted chunk at coord: {},{}".format(x, y)


if __name__ == '__main__':
    main()
